import requests
import os
import time
import yaml
import json


def create_user(admin_credentials, user_data, admin_token_url, new_user_url, cert):
    # get admin token
    admin_token = requests.post(url=admin_token_url, data=admin_credentials, verify=cert).json()
    # create user
    header = {
        "Authorization": "Bearer " + admin_token["access_token"],
        "Content-Type": "application/json"}
    response = requests.post(url=new_user_url, json=user_data, verify=False, headers=header)
    return response


def get_access_token_from_keycloak(hostname, credentials, cert):
    credentials["grant_type"] = "password"
    response = requests.post(url=hostname, data=credentials, verify=cert).json()
    print("Keycloak response:")
    print(yaml.dump(response))
    return response


def get_workflow_data():
    with open(os.path.normpath("tests/workflows/wf1/config.yaml")) as file:
        workflow_params = json.dumps(yaml.load(file, Loader=yaml.FullLoader))
    snakefile = "file:wf1/Snakefile"
    data = {
        "workflow_params": workflow_params,
        "workflow_type": "SMK",
        "workflow_type_version": "6.10.0",
        "workflow_url": snakefile
    }
    return data


def run_demo(host, cert, header):
    weskit_host = host

    print("****************************************")
    print("GET:/ga4gh/wes/v1/service-info")
    response1 = requests.get("%s/ga4gh/wes/v1/service-info" % (weskit_host), verify=cert)
    print("Status Code:", response1.status_code)
    print("Response:", response1.json())
    print("****************************************\n\n")
    print("POST:/ga4gh/wes/v1/runs")
    response2 = requests.post("%s/ga4gh/wes/v1/runs" % (weskit_host),
                              data=get_workflow_data(), verify=cert, headers=header)
    print(response2.status_code)
    print(response2.json())
    print("****************************************")
    print("GET:/ga4gh/wes/v1/runs")
    response3 = requests.get("%s/ga4gh/wes/v1/runs" % (weskit_host), verify=cert, headers=header)
    print("Status Code:", response3.status_code)
    print("Response:")
    print(yaml.dump(response3.json()))
    print("****************************************\n\n")
    print("****************************************")
    print("GET:/ga4gh/wes/v1/runs/%s/status" % (response2.json()["run_id"]))
    response4 = requests.get("{}/ga4gh/wes/v1/runs/{}/status".
                             format(weskit_host, response2.json()["run_id"]),
                             verify=cert, headers=header)
    success = False
    while not success:
        status = response4.json()["state"]
        if status != "COMPLETE":
            print("Waiting ... (status=%s)" % status)
            time.sleep(1)
            response4 = requests.get("{}/ga4gh/wes/v1/runs/{}/status".
                                     format(weskit_host, response2.json()["run_id"]),
                                     verify=cert, headers=header)
            continue
        success = True
    print("****************************************\n\n")
    print("****************************************")
    print("GET:/ga4gh/wes/v1/runs/%s" % (response2.json()["run_id"]))
    response5 = requests.get("{}/ga4gh/wes/v1/runs/{}".
                             format(weskit_host, response2.json()["run_id"]),
                             verify=cert, headers=header)
    print("Status Code:", response5.status_code)
    print(yaml.dump(response5.json()))
    print("****************************************\n\n")
